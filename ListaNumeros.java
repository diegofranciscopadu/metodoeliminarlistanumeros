/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import java.util.Arrays;

/**
 * Nuestra primera estructura : Vector--> ListaNumeros
 *
 * @author madarme
 */
public class ListaNumeros implements Comparable {

    private float[] numeros;

    public ListaNumeros() {
    }

    public ListaNumeros(int cant) {
        if (cant <= 0)
        {
            throw new RuntimeException("No se puede crear el vector");
        }

        this.numeros = new float[cant];

    }

    /**
     * Adiciona un número en la posición i
     *
     * @param i indice donde se va a ingresar el dato
     * @param numeroNuevo el dato a ingresar
     */
    public void adicionar(int i, float numeroNuevo) {
        this.validar(i);
        this.numeros[i] = numeroNuevo;
    }



    /**
     * Elimina la posición i del vector y REDIMENSIONA EL VECTOR Osea elimina
     * por indice
     *
     * @param indiceRemover la posición a eliminar
     */
    public void eliminar(int indiceRemover) {
        this.validar(indiceRemover);

        float[] otroArray = new float[this.numeros.length - 1];

        for (int i = 0, k = 0; i < this.numeros.length; i++)
        {

            if (i != indiceRemover)
            {
                otroArray[k++] = this.numeros[i];
            }

        }

        this.numeros = otroArray;

    }


    public float getElemento(int i) {

        this.validar(i);
        return this.numeros[i];
    }

    private void validar(int i) {
        if (i < 0 || i >= this.numeros.length)
        {
            throw new RuntimeException("Índice fuera de rango:" + i);
        }
    }

    /*
        ES REDUNDANTE
    **/
    public void actualizar(int i, float numeroNuevo) {
        this.adicionar(i, numeroNuevo);
    }

    public float[] getNumeros() {
        return numeros;
    }

    public void setNumeros(float[] numeros) {
        this.numeros = numeros;
    }

    @Override
    public String toString() {
        String msg = "";
        /*
            Cuando sólo son recorridos, usamos el foreach
            for(T elemento:coleccion)
                hacer algo con elemento;
         */

        for (float dato : this.numeros)
        {
            msg += dato + "\t";
        }
        return msg;
    }

    public int length() {
        return this.numeros.length;
    }

    /**
     * Ordena el vector por el método de la burbuja
     */
    public void ordenar_Burbuja() {
        {
            int n = this.numeros.length;
            for (int i = 0; i < n - 1; i++)
            {
                for (int j = 0; j < n - i - 1; j++)
                {
                    if (this.numeros[j] > this.numeros[j + 1])
                    {
                        // swap arr[j+1] and arr[j]
                        int temp = (int) this.numeros[j];
                        this.numeros[j] = this.numeros[j + 1];
                        this.numeros[j + 1] = temp;
                    }
                }
            }
        }
    }

    /**
     * Ordena el vector por el método de selección
     */
    public void ordenar_Seleccion() {
        int n = this.numeros.length;

        // One by one move boundary of unsorted subarray
        for (int i = 0; i < n - 1; i++)
        {
            // Find the minimum element in unsorted array
            int min_idx = i;
            for (int j = i + 1; j < n; j++)
            {
                if (this.numeros[j] < this.numeros[min_idx])
                {
                    min_idx = j;
                }
            }

            // Swap the found minimum element with the first
            // element
            int temp = (int) this.numeros[min_idx];
            this.numeros[min_idx] = this.numeros[i];
            this.numeros[i] = temp;
        }
    }

    /**
     * Retorna una lista de numeros con la unión de conjuntos de lista original
     * con lista dos.
     *
     * Se debe validar que la listanueva contenga UNICAMENTE LA CANTIDAD DE
     * CELDAS REQUERIDAS
     *
     * Ejemplo: this={3,4,5,6} y dos={3,4} ListaNueva = {3,4,5,6}
     *
     * @param dos una lista de numeros
     * @return una nueva lista con la unión de conjuntos
     */
    public ListaNumeros getUnion(ListaNumeros dos) {

        // :)
        return null;
    }

    /**
     * Retorna una lista de numeros con la INTERSECCION de conjuntos de lista
     * original con lista dos.
     *
     * Se debe validar que la listanueva contenga UNICAMENTE LA CANTIDAD DE
     * CELDAS REQUERIDAS
     *
     * Ejemplo: this={3,4,5,6} y dos={3,4} ListaNueva = {3,4}
     *
     * @param dos una lista de numeros
     * @return una nueva lista con la unión de conjuntos
     */
    public ListaNumeros getInterseccion(ListaNumeros dos) {

        int contador = 0;
        int x = 0;
        ListaNumeros interseccion = new ListaNumeros(dos.length() + this.numeros.length);

        for (int i = 0; i < this.numeros.length; i++)
        {
            for (int j = 0; j < dos.length(); j++)
            {
                if (dos.getElemento(i) == this.numeros[j])
                {
                    contador++;
                    interseccion.adicionar(contador, dos.getElemento(i));

                }
            }
        }//averiguar el indice de los repetidos
        int[] ar =
        {
            1, 2, 2, 1, 1, 3, 5, 1, 2
        };
        Arrays.sort(ar);
        int contadorEliminar = 0;
        int aux = ar[0];
        for (int i = 0; i < ar.length; i++)
        {
            if (aux == ar[i])
            {
                contador++;
            } else
            {
                System.out.print(contador + ",");
                contador = 1;
                aux = ar[i];
            }
        }
        System.out.print(contador);

        for (int i = 0; i < interseccion.length(); i++)
        {
            System.out.println("INTERSECCION ==" + interseccion.getElemento(i));
        }

        return dos;

//        //m == numeros, n==dos
//         boolean bandera = false;    
//        ListaNumeros interseccion = new ListaNumeros(this.numeros.length+dos.length());
//        int contador = 0;
//        for (int i = 0, aux = 0; i < this.numeros.length; i++) {            
//            for (int j = 0; j < dos.length(); j++) {   
//                if (this.numeros[i] == dos.getElemento(j)) {
//                    bandera = false;            
//                    for (int sal = 0; sal < dos.length(); sal++) {
//                        if (interseccion.getElemento(sal) == dos.getElemento(j)) {
//                            bandera = true;
//                            sal=+dos.length()-1;
//                        }               
//                    }   
//                    if(!bandera){
//                        interseccion.adicionar(aux, dos.getElemento(j));
//                        aux++;    
//                        contador++;
//                    }
//                }
//            }
//        }    
//    
//        ListaNumeros salida2 = new ListaNumeros(contador);
//        for(int i =0;i<salida2.length();i++){
//            salida2.adicionar(i, interseccion.getElemento(i));
//        }
//  
//        return salida2;
//        
//        ListaNumeros intersect = new ListaNumeros(this.numeros.length + dos.length());  
//     
//        int i = 0, j = 0;
//        while (i < this.numeros.length && j < dos.length()) {
//            if (this.numeros[i] < dos.getElemento(j))
//                i++;
//            else if (dos.getElemento(j) < this.numeros[i])
//                j++;
//            else {
//                System.out.print(dos.getElemento(j++) + " ");
//                i++;
//            }
//        }
//        
//        
//        
//   
//        
//        
//        
//        
//        //   ListaNumeros intersect = new ListaNumeros(this.numeros.length));
//        int c = 0;
//        ListaNumeros intersect = new ListaNumeros(this.numeros.length + dos.length());
//        for (int i = 0; i < (this.numeros.length); i++)
//        {
//            for (int j = 0; j < dos.length(); j++)
//            {
//                if (this.numeros[i] == dos.getElemento(j))
//                {
//                 this.numeros[i]=  intersect.getElemento(c) ;
//
//                    c++;
//                } else
//                {
//                    continue;
//                }
//
//            }
//        }
//
//        for (int i = 0; i < intersect.length(); i++)
//        {
//            System.out.println("LA INTERSECION" + intersect.getElemento(i));
//        }
//       return intersect;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Arrays.hashCode(this.numeros);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final ListaNumeros other = (ListaNumeros) obj;

        if (this.numeros.length != other.numeros.length)
        {
            return false;
        }

        for (int i = 0; i < this.numeros.length; i++)
        {
            if (this.numeros[i] != other.numeros[i])
            {
                return false;
            }
        }
        return true;
    }

    /**
     * Una lista es mayor a otra si y solo si: 1. Tienen el mismo tamaño 2.
     * Todos los elementos de la lista1 son mayores a los de la lista 2 en orden
     * ( suponga que la lista 1 y 2 se encuentra ordenada)
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(Object o) {
        if (this == o)
        {
            return 0;
        }
        if (this == null)
        {
            throw new RuntimeException("No se puede comparar con el segundo objeto si es nulo");
        }
        if (getClass() != o.getClass())
        {
            throw new RuntimeException("Los objetos son de distinta clase");
        }
        final ListaNumeros other = (ListaNumeros) o;
        //pendiente revisar el casting    

        return this.numeros.length - other.length();

    }

}
