/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.ListaNumeros;
import java.util.Scanner;

/**
 *
 * @author madar
 */
public class TestVectores {

        public static void main(String[] args) {

            ListaNumeros lista = new ListaNumeros(leerEntero("Digite cantidad de elementos para lista"));
     //       ListaNumeros segundaLista = new ListaNumeros(leerEntero("Digite cantidad de elementos para lista"));
            /**
             * Como vamos a realizar un proceso de almacenamiento usamos el for
             * convencional
             */
            for (int i = 0; i < lista.length(); i++) {
                lista.adicionar(i, leerFloat("Digite dato [" + i + "]: "));  
            }
                //COMPROBANDO SELECCION
//           System.out.println("Su lista es:" + lista.toString());
//            lista.ordenar_Seleccion();
//           System.out.println("Su lista es:" + lista.toString());
                //COMPROBANDO SELECCION
                
                //COMPROBANDO BURBUJA
//           System.out.println("Su lista es:" + lista.toString());
//            lista.ordenar_Burbuja();
//           System.out.println("Su lista es:" + lista.toString());
                //COMPROBANDO BURBUJA
                
                //COMPROBANDO ELIMINAR
            System.out.println("Su lista es:" + lista.toString());
            int datoEliminar = (int)leerFloat("Escriba el indice del elemento que quiere eliminar: ");
            lista.eliminar(datoEliminar);
            System.out.println("Su lista es:" + lista.toString());
                //COMPROBANDO ELIMINAR

//            for (int i = 0; i < segundaLista.length(); i++) {
//                segundaLista.adicionar(i, leerFloat("Digite dato [" + i + "]: "));  
//            }
            
         //   float numerosFLoat[] = {1,2,3,4};
  //    lista.getInterseccion(segundaLista);
         //     System.out.println(lista.compareTo(segundaLista)); 
        }

    private static float leerFloat(String msg) {
        System.out.print(msg);
        Scanner in = new Scanner(System.in);
        try {
            return in.nextFloat();
        } catch (java.util.InputMismatchException ex) {
            System.err.println("Error no es un Float");
            return leerFloat(msg);
        }

    }

    private static int leerEntero(String msg) {
        System.out.print(msg);
        Scanner in = new Scanner(System.in);
        try {
            return in.nextInt();
        } catch (java.util.InputMismatchException ex) {
            System.err.println("Error no es un Float");
            return leerEntero(msg);
        }

    }

}
